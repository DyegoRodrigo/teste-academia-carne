# -*- coding: latin-1 -*-

'''
Created on May 6, 2016

@author: richard
'''
import webapp2
import logging
from xlsxwriter.workbook import Workbook
from google.appengine.ext.webapp.util import run_wsgi_app
import io
import ftplib
import sys
import urllib2
from urllib2 import FTPHandler
import urllib

class TestExcel(webapp2.RequestHandler):

    def get(self):
        logging.info("Starting excel generation")
        output = io.BytesIO()
        try: 
            workbook = Workbook(output, {'in_memory': True})
            worksheet = workbook.add_worksheet()
            worksheet.write(0, 0, 'Hello, world!')
            workbook.close()

            # to finish buffer....
            output.seek(0)

            #WRITE to a file in binary mode...
            with io.open('/home/dyego/Desktop/myfile.xlsx', 'wb') as f:
                f.write(output.getvalue())

            #close buffer....
            f.close() 

            self.response.write('done')
            logging.info("Excel generation done")
        except Exception as e:
           logging.error("Error when generating excel: " + str(e))



           # references of xlsxwriter my be founded on: http://xlsxwriter.readthedocs.io/index.html

class FtpUploader(webapp2.RequestHandler):

    def get(self):
        login = "jbsacademia_us"
        passwd = "asghais23456*&#"
        host = "ftp://54.243.25.154"

#         req = urllib2.Request('ftp://jbsacademia_us:asghais23456*&#@54.243.25.154', 21)
#         response = urllib2.urlopen(req)
#         the_page = response.read()
#         
#         print the_page

        ftp = ftplib.FTP(host)
        ftp.set_pasv(False)
        self.response.write(ftp.getwelcome())

        try:
            print "Logging in..."
            ftp.login(login, passwd)
            self.response.write('<br> Logged')
            print "logged"
            print ftp.pwd()
            ftp.cwd(ftp.pwd())
            file1 = open('/Users/richard/Documents/Technical/workspace/TestAcademiadaCarne/src/myfile_t.xlsx', "rb")
            ftp.set_pasv(False)
            print ftp.retrlines('LIST') 
            #print ftp.transfercmd('STOR myfile_t.xlsx') 
            #print ftp.storbinary('STOR myfile_t.xlsx', file1)
            print 'file sent'
        except:
            e = sys.exc_info()
            #logging.error('Error unexpected: [%s]', e)
            print e
        finally:
            file1.close()
            ftp.quit()
            ftp.close()

        file1 = open('/Users/richard/Documents/Technical/workspace/TestAcademiadaCarne/src/myfile_t.xlsx','rb') # file to send
        ftp.storbinary('file.xlsx', file1)     # send the file
        file.close()                                    # close file and FTP
        ftp.quit()




application = webapp2.WSGIApplication([
    webapp2.Route('/test_excel', TestExcel),
    webapp2.Route('/test_ftp', FtpUploader)
    ], debug=True)

def main():
    run_wsgi_app(application)

if __name__ == "__main__":
    main()


logging.getLogger().setLevel(logging.DEBUG)
